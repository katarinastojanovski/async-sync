﻿using Services;
using System.ServiceModel;
using System.Threading;

namespace Wcf
{
    [ServiceBehavior(UseSynchronizationContext = true, InstanceContextMode = InstanceContextMode.PerCall, ConcurrencyMode = ConcurrencyMode.Multiple)]
    public class StringService : IStringService
    {
        private readonly StringRetriever stringRetriever;

        public StringService()
        {
            this.stringRetriever = new StringRetriever();
        }

        public string GetString()
        {
            var synchronizationContext = SynchronizationContext.Current;
            var task = this.stringRetriever.GetMyString();
            var result = task.Result;
            return result;
        }

        public string GetStringWithConfigureAwait()
        {
            var task = this.stringRetriever.GetMyStringWithConfigureAwait();
            var result = task.Result;
            return result;
        }
    }
}
