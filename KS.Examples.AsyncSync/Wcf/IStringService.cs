﻿using System.ServiceModel;

namespace Wcf
{
    [ServiceContract]
    public interface IStringService
    {
        [OperationContract]
        string GetString();

        [OperationContract]
        string GetStringWithConfigureAwait();
    }
}
