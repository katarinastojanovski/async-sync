﻿using System.Net.Http;
using System.Threading.Tasks;

namespace Services
{
    public class StringRetriever
    {
        public async Task<string> GetMyStringWithConfigureAwait()
        {
            using (var client = new HttpClient())
            {
                var jsonString = await client.GetStringAsync("http://www.google.com").ConfigureAwait(false);
                return jsonString;
            }
        }

        public async Task<string> GetMyString()
        {
            using (var client = new HttpClient())
            {
                var jsonString = await client.GetStringAsync("http://www.google.com");
                return jsonString;
            }
        }
    }
}
