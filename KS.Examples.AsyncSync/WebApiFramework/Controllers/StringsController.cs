﻿using Services;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;

namespace WebApiFramework.Controllers
{
    [RoutePrefix("api/strings")]
    public class StringsController : ApiController
    {
        private readonly StringRetriever stringRetriever;

        public StringsController(StringRetriever stringRetriever)
        {
            this.stringRetriever = stringRetriever;
        }

        [Route("sync")]
        public IHttpActionResult GetString()
        {
            var synchronizationContext = SynchronizationContext.Current;
            var task = this.stringRetriever.GetMyString();
            var result = task.Result;
            return this.Ok(result);
        }

        [Route("syncconfigureawait")]
        public IHttpActionResult GetStringWithConfigureAwait()
        {
            var task = this.stringRetriever.GetMyStringWithConfigureAwait();
            var result = task.Result;
            return this.Ok(result);
        }

        [Route("async")]
        public async Task<IHttpActionResult> GetStringAsync()
        {
            var result = await this.stringRetriever.GetMyString();
            return this.Ok(result);
        }

        [Route("asyncconfigureawait")]
        public async Task<IHttpActionResult> GetStringWithConfigureAwaitAsync()
        {
            var result = await this.stringRetriever.GetMyString();
            return this.Ok(result);
        }
    }
}