﻿using System.Web.Http;
using System.Web.Http.Dispatcher;

namespace WebApiFramework
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            config.Services.Replace(
                typeof(IHttpControllerActivator),
                new CustomHttpControllerActivator());
            config.MapHttpAttributeRoutes();
        }
    }
}