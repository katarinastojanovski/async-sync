﻿using Services;
using System;
using System.Net.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Dispatcher;
using WebApiFramework.Controllers;

namespace WebApiFramework
{
    public class CustomHttpControllerActivator : IHttpControllerActivator
    {
        public IHttpController Create(
            HttpRequestMessage request,
            HttpControllerDescriptor controllerDescriptor,
            Type controllerType)
        {
            if (controllerType == typeof(StringsController))
            {
                return new StringsController(
                    new StringRetriever());

            }

            return null;
        }
    }
}