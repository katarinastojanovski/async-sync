﻿using Microsoft.AspNetCore.Mvc;
using Services;
using System.Threading;
using System.Threading.Tasks;

namespace WebApiCore.Controllers
{
    [Route("api/[controller]")]
    public class StringsController : ControllerBase
    {
        private readonly StringRetriever stringRetriever;

        public StringsController(StringRetriever stringRetriever)
        {
            this.stringRetriever = stringRetriever;
        }

        [Route("sync")]
        public ActionResult<string> GetString()
        {
            var synchronizationContext = SynchronizationContext.Current;
            var task = this.stringRetriever.GetMyString();
            var result = task.Result;
            return this.Ok(result);
        }

        [Route("syncconfigureawait")]
        public ActionResult<string> GetStringWithConfigureAwait()
        {
            var task = this.stringRetriever.GetMyStringWithConfigureAwait();
            var result = task.Result;
            return this.Ok(result);
        }

        [Route("async")]
        public async Task<ActionResult<string>> GetStringAsync()
        {
            var result = await this.stringRetriever.GetMyString();
            return this.Ok(result);
        }

        [Route("asyncconfigureawait")]
        public async Task<ActionResult<string>> GetStringWithConfigureAwaitAsync()
        {
            var result = await this.stringRetriever.GetMyString();
            return this.Ok(result);
        }
    }
}
